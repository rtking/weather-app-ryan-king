## Ryan King Weather App Challenge Readme

This weather application has been made to enable the user to pick a location via a UITextfield and then scroll through a series of 40 forecasts for the next 5 days.
The app is made up of 2 modules, the Search Bar View Map View, which are on top of the Main View Controller which handles the network call and UITableview.

The application is not entirely finished, a few problems include :
   - Filter picks the first city name that matches the users input, this is incorrect as there are city names are not unique.
   - Using the network call which requires sending the city ID was not ideal, it was quite late in the project I realised that it meant I would need to create 219,000 objects.
   
Some improvements I would make if given more time :
   - Fix issues previously mentioned.
   - Add suggestions to user as they type out location in searchbar, including different options for various countries.
   - Introduce delegate pattern for modules to replace NotificationCenter notifications.
   - Turn UITableview into seperate module


Time taken : 7 hours 45 minutes.