//
//  WACConstants.swift
//  ryan-king-weather-app-challenge
//
//  Created by Ryan on 27/02/17.
//  Copyright © 2017 Ryan-King. All rights reserved.
//

import Foundation

class WACConstants {
    static let openWeatherMapAPI = "5830b4cbbc4dbf80d54f8969d0863baa"
    static let openWeatherAPIBaseURL = "http://api.openweathermap.org/data/2.5/forecast/city"
    static let userInputNotification = Notification.Name("userHasInputCity")
    static let cityArrayReadyNotification = Notification.Name("cityArrayReady")
    static let setMapViewNotification = Notification.Name("setMapView")
    static let WeatherTableViewCellHeight = 64.0
    static let viewCornerRadius = 8.0
    static let cityViewSpan = 10000.0
}
