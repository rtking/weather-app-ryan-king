//
//  WACHTTPClient.swift
//  ryan-king-weather-app-challenge
//
//  Created by Ryan on 27/02/17.
//  Copyright © 2017 Ryan-King. All rights reserved.
//

import Foundation

class WACHTTPClient {
    
    // Function gets the five day weather forecast for a particular city from the Open Weather Map API
    static func getFiveDayWeatherForecastForCity(cityID : NSInteger, completionHandler: @escaping (Data?) -> Swift.Void) {
        // Create the string and URL
        let weatherAPIEndpoint = String(format: "%@?id=%ld&APPID=%@", WACConstants.openWeatherAPIBaseURL,cityID, WACConstants.openWeatherMapAPI)
        let weatherURL = URL(string: weatherAPIEndpoint)
        
        // Make the network call
        let task = URLSession.shared.dataTask(with: weatherURL! as URL) {
            data, response, error in
            // Make sure we have no errors
            guard error == nil else {
                print(error!)
                return
            }
            // Make sure we have data
            guard let data = data else {
                print("Data is empty")
                return
            }
            // Fire completionHandler
            completionHandler(data)
        }
        task.resume()
    }
}
