//
//  WACWeatherTableViewCell.swift
//  ryan-king-weather-app-challenge
//
//  Created by Ryan on 27/02/17.
//  Copyright © 2017 Ryan-King. All rights reserved.
//

import UIKit

class WACWeatherTableViewCell : UITableViewCell {
    // Outlets
    @IBOutlet var weatherImageView : UIImageView!
    @IBOutlet var weatherDescriptionLabel : UILabel!
    @IBOutlet var temperatureDescriptionLabel : UILabel!
    @IBOutlet var dayLabel : UILabel!
    @IBOutlet var timeLabel : UILabel!
}
