//
//  WACForecast.swift
//  ryan-king-weather-app-challenge
//
//  Created by Ryan on 28/02/17.
//  Copyright © 2017 Ryan-King. All rights reserved.
//

import Foundation

class WACForecast {
    var temperature : NSInteger!    // Degrees celcius
    var weather : String!           // Description string
    var wind : NSInteger!           // Wind speed in KPH
    var date : NSDate!
    
    // Failable initializer
    public init?(json: [String: Any]) {
        
        // Set the date
        let date = json["dt"] as? Double
        
        // Set the temperature
        let mainDict = json["main"] as! [String: Any]
        let temperature = mainDict["temp"] as? NSInteger
        
        // Set the wind
        let windDict = json["wind"] as! [String: Any]
        let wind = windDict["speed"] as? NSInteger
        
        // Set the weather
        let weatherArray = json["weather"] as! NSMutableArray
        let weatherDict = weatherArray.firstObject as! [String: Any]
        let weather = weatherDict["main"] as? String
        
        // Convert from Kelvin into degrees (to an rough accuracy of 0 d.p)
        self.temperature = temperature! - 273
        // Remove the quotation marks
        self.weather = weather?.replacingOccurrences(of: "\"", with: "")
        self.wind = wind
        self.date = NSDate(timeIntervalSince1970: date!)
        
        // Return nil if we do not have all attributes
        if (temperature != nil && weather != nil && wind != nil && date != nil) {
            self.temperature = temperature! - 273
            self.weather = weather
            self.wind = wind
            self.date = NSDate(timeIntervalSince1970: date!)
        } else {
            return nil
        }
    }
    
    static func getJSONArrayFromResponseData(jsonData : Data?) -> NSMutableArray {
        var dictionaryArray = NSMutableArray()
        
        do {
            let jsonResults = try JSONSerialization.jsonObject(with: jsonData!, options: []) as! [String:Any]
            if (jsonResults["list"] != nil) {
                dictionaryArray = jsonResults["list"] as! NSMutableArray
            }
        } catch let error {
            // Something has gone wrong with our deserialization, print the error out
            print(error.localizedDescription)
        }
        return dictionaryArray
    }
    
    static func takeJSONArrayAndCreateArrayOfForecasts(jsonResults : NSMutableArray) -> NSMutableArray {
        let forecastArray = NSMutableArray()
        // Loop through all jsonResults and for every one, create a city object and add it to array
        for i in (0...jsonResults.count-1) {
            if let forecast = WACForecast(json: jsonResults.object(at: i) as! [String:Any]) {
                forecastArray.add(forecast)
            }
        }
        return forecastArray
    }
}
