//
//  WACSearchBar.swift
//  ryan-king-weather-app-challenge
//
//  Created by Ryan on 27/02/17.
//  Copyright © 2017 Ryan-King. All rights reserved.
//

import UIKit
import Foundation

class WACSearchBarView : UIView, UITextFieldDelegate {
    // Outlets
    @IBOutlet var searchTextfield : UITextField!
    
    // Variables
    var cityArrayIsReady : Bool!
    var viewSetupComplete : Bool!
    
    // MARK: VIEW LIFECYCLE METHODS
    
    override func awakeFromNib() {
        // Ensure that we only go through this setup once
        if viewSetupComplete == nil {
            // Round the corners of our view
            self.layer.cornerRadius = CGFloat(WACConstants.viewCornerRadius)
        
            // Register to receive notification
            NotificationCenter.default.addObserver(self, selector: #selector(self.recievedNotification), name: WACConstants.cityArrayReadyNotification, object: nil)
            
            // Set our view setup bool so we don't repeat
            self.viewSetupComplete = true
            self.cityArrayIsReady = false
        }
    }
    
    func recievedNotification() {
        cityArrayIsReady = true
    }
    
    // MARK: UITEXTFIELD METHODS
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if cityArrayIsReady == true {
            // Post notification back to MainViewController
            let cityDict:[String: String] = ["city": textField.text!]
            NotificationCenter.default.post(name: WACConstants.userInputNotification, object: nil, userInfo: cityDict)
            textField.resignFirstResponder();
        }
        return true;
    }
}
