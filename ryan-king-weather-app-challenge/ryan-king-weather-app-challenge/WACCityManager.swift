//
//  WACCityManager.swift
//  ryan-king-weather-app-challenge
//
//  Created by Ryan on 28/02/17.
//  Copyright © 2017 Ryan-King. All rights reserved.
//

import Foundation

class WACSingletonManager {
    static let sharedInstance = WACSingletonManager()
    // Global city array
    var cityArray : NSMutableArray!
}
