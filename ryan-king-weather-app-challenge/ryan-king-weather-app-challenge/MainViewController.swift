//
//  ViewController.swift
//  ryan-king-weather-app-challenge
//
//  Created by Ryan on 27/02/17.
//  Copyright © 2017 Ryan-King. All rights reserved.
//

import UIKit

class MainViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    // Outlets
    @IBOutlet var searchBarView : UIView!
    @IBOutlet var mapView : UIView!
    @IBOutlet var weatherTableView : UITableView!
    
    // Variables
    var forecasts : NSMutableArray!
    var currentLocation : WACCity!
    
    // MARK: VIEW LIFECYCLE METHODS
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setup forecasts empty array
        self.forecasts = NSMutableArray()
        
        // Register to receive notification for input in searchbar
        NotificationCenter.default.addObserver(self, selector: #selector(self.userHasInputCity), name: WACConstants.userInputNotification, object: nil)
        
        // Register custom UITableView class
        self.weatherTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        // Stop listening notification
        NotificationCenter.default.removeObserver(self, name: WACConstants.userInputNotification, object: nil);
    }
    
    // MARK: USER INPUT METHODS
    
    func userHasInputCity(_ notification: NSNotification) {
        // Check that the city is not nil
        if let city = notification.userInfo?["city"] as? String {
            
            // Find the city in city array
            currentLocation = WACCity.findLocationInArray(cityArray: WACSingletonManager.sharedInstance.cityArray, location: city)
            
            // Ensure that current location is not nil
            if currentLocation != nil {
                
                // Post notification to mapView
                let cityDict:[String: WACCity] = ["city": currentLocation]
                NotificationCenter.default.post(name: WACConstants.setMapViewNotification, object: nil, userInfo: cityDict)
                
                // Send network call
                WACHTTPClient.getFiveDayWeatherForecastForCity(cityID: currentLocation.id) { (data) in
                    // Function returns a list of JSON dictionaries
                    let jsonResults = WACForecast.getJSONArrayFromResponseData(jsonData: data!)
                    
                    // Create array of forecasts
                    self.forecasts = WACForecast.takeJSONArrayAndCreateArrayOfForecasts(jsonResults: jsonResults)
                    
                    // Reload our table in the main thread, as UIKit is not thread safe
                    DispatchQueue.main.async {
                        self.weatherTableView.reloadData()
                    }
                }
            } else {
                // Create alert controller to inform the user of invalid city
                let alert = UIAlertController(title: "Invalid City", message: "Sorry we are unable to find your specified city, please try again", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    // MARK: UITABLEVIEW METHODS
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(WACConstants.WeatherTableViewCellHeight)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.forecasts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Instantiate the custom cell
        let cell : WACWeatherTableViewCell = self.weatherTableView.dequeueReusableCell(withIdentifier: "weatherCell") as! WACWeatherTableViewCell
        
        // Get the forecast out of the array
        let forecast = self.forecasts.object(at: indexPath.row) as! WACForecast
        
        // Set the labels to correct values
        cell.weatherDescriptionLabel.text  = "\(forecast.weather!), \(forecast.wind!)KPH"
        cell.temperatureDescriptionLabel.text = "\(forecast.temperature!)"
        
        // Format our date to a readable string
        let myFormatter = DateFormatter()
        myFormatter.dateStyle = .short
        cell.dayLabel.text = myFormatter.string(from: forecast.date! as Date)
        
        // Format our time to a readable string
        myFormatter.dateFormat = "HH:mm"
        cell.timeLabel.text = myFormatter.string(from: forecast.date! as Date)
        
        switch forecast.weather! {
        case "Snow":
            cell.weatherImageView.image = #imageLiteral(resourceName: "snow")
        case "Rain":
            cell.weatherImageView.image = #imageLiteral(resourceName: "rain")
        case "Clouds":
            cell.weatherImageView.image = #imageLiteral(resourceName: "clouds")
        case "Clear":
            cell.weatherImageView.image = #imageLiteral(resourceName: "clear")
        default:
            cell.weatherImageView.image = nil
        }
        
        return cell
    }
    
    // MARK: MEMORY MANAGEMENT

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

