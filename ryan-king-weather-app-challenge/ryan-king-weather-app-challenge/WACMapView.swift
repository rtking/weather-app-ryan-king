//
//  WACMapView.swift
//  ryan-king-weather-app-challenge
//
//  Created by Ryan on 27/02/17.
//  Copyright © 2017 Ryan-King. All rights reserved.
//

import UIKit
import MapKit

class WACMapView : UIView {
    // Outlets
    @IBOutlet var mkMapView : MKMapView!
    
    // Variables
    var viewSetupComplete : Bool!
    
    // MARK: VIEW LIFECYCLE METHODS
    
    override func awakeFromNib() {
        // Ensure that we only go through this setup once
        if viewSetupComplete == nil {
            // Round the corners of our view
            self.layer.cornerRadius = CGFloat(WACConstants.viewCornerRadius)
            
            // Register to receive notification
            NotificationCenter.default.addObserver(self, selector: #selector(self.setMapView), name: WACConstants.setMapViewNotification, object: nil)
            
            // Set our view setup bool so we don't repeat
            self.viewSetupComplete = true
        }
    }
    
    // MARK: MAP METHODS
    
    func setMapView(_ notification: NSNotification) {
        // Check that the city is not nil
        if let city = notification.userInfo?["city"] as? WACCity {
            if (city.longitude != nil && city.latitude != nil) {
                let location = CLLocationCoordinate2DMake(city.latitude, city.longitude)
                self.mkMapView.setRegion(MKCoordinateRegionMakeWithDistance(location, WACConstants.cityViewSpan, WACConstants.cityViewSpan), animated: true)
            }
        }
    }
}
