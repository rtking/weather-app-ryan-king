//
//  WACCity.swift
//  ryan-king-weather-app-challenge
//
//  Created by Ryan on 27/02/17.
//  Copyright © 2017 Ryan-King. All rights reserved.
//

import Foundation

class WACCity {
    var id : NSInteger!
    var name : String!
    var country : String!
    var longitude : Double!
    var latitude : Double!
    
    // Failable initializer
    public init?(json: [String: Any]) {
        
            let id = json["_id"] as? NSInteger
            let name = json["name"] as? String
            let country = json["country"] as? String
            let coord = json["coord"] as! [String: Any]
            let longitude = coord["lon"] as? Double
            let latitude = coord["lat"] as? Double
        
            // Return nil if we do not have all attributes
            if (id != nil && name != nil && country != nil && longitude != nil && latitude != nil) {
                self.id = id
                self.name = name
                self.country = country
                self.longitude = longitude
                self.latitude = latitude
            } else {
                return nil
            }
    }
    
    static func getJSONArrayFromLocalCityData() -> NSMutableArray {
        var jsonResults = NSMutableArray()
        
        if let path = Bundle.main.path(forResource: "local_city_list", ofType: "json") {
            do {
                // Turn the JSON into data, data into NSMutableArray
                let jsonData = try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
                jsonResults = try JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableArray
                
            } catch let error {
                // Something has gone wrong with our deserialization, print the error out
                print(error.localizedDescription)
            }
        } else {
            print("Invalid filename/path.")
        }
        return jsonResults
    }
    
    static func takeJSONArrayAndCreateArrayOfCities(jsonResults : NSMutableArray) -> NSMutableArray {
        let cityArray = NSMutableArray()
        // Loop through all jsonResults and for every one, create a city object and add it to array
        for i in (0...jsonResults.count-1) {
            if let city = WACCity(json: jsonResults.object(at: i) as! [String:Any]) {
                cityArray.add(city)
            }
        }
        return cityArray
    }
    
    static func findLocationInArray(cityArray : NSMutableArray, location : String) -> WACCity? {
        // Retrieve the first city that has a matching name
        if let city = cityArray.filter({($0 as! WACCity).name == location}).first {
            return city as? WACCity
        } else {
            return nil
        }
    }
}
